﻿using Domain;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebMVC.Tests.Controllers
{

    [TestClass]
    public class CalculatorTest
    {
        
        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }

        private Calculator _SystemUnderTest;

        public Calculator SystemUnderTest
        {
            get
            {
                if(_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }
                return _SystemUnderTest;
            }


        }



 
        [TestMethod]
        public void Add()
        {

            //Arrange (Organizar)
            int value1 = 2;
            int value2 = 3;
            int expected = 5;

            //Act (Actuar)


            int actual = SystemUnderTest.Add(value1,value2);

            //Assert (Afirmar)
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Restar()
        {
            //Arrange
            int value3 = 3;
            int value4 = 2;
            int expected = 1;

            //Act
            
            int actual = SystemUnderTest.Restar(value3,value4);

            //Assert
            Assert.AreEqual<int>(expected, actual, "Error, valores no coinciden");
        }

        [TestMethod]
        public void Multi()
        {
            //Arranque
            int value5 = 2;
            int value6 = 2;
            int expected = 104;

            //Actuar
            int actual = SystemUnderTest.Multi(value5, value6);

            //Assert
            Assert.AreNotEqual(expected, actual, "Error, valores no coinciden");

        }

        [TestMethod]
        public void Dividir()
        {
            //Arranque
            int value7 = 12;
            int value8 = 2;
            int expected = 6;

            //Actuar
            int actual = SystemUnderTest.Dividir(value7, value8);

            //Assert
            Assert.AreNotEqual(expected, actual, "Error, valores no coinciden");

        }
    }
}
//Integrantes: Tonny Cruz, Meyling Mendoza, Grethel Fonseca, Carlos Diaz
//Universidad Centroamericana
