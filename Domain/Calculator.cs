﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Calculator
    {
        public int Add(int value1, int value2)
        {
            return value1 + value2;
        }

        public int Restar(int value3, int value4)
        {
            return value3 - value4;
        }

        public int Multi(int value5, int value6)
        {
            return value5 * value6;
        }

        public int Dividir(int value7, int value8)
        {
            return value7 / value8;
        }
    }
}
